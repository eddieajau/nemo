# CLI: Create a list of tasks

As a user
I need to be able to lists the tasks I have
So that I know what to do next.

## Accecptance Criteria

1. Create a command-line script to show a list of tasks using the following object model:
   1. `id`: number
   2. `title`: string
2. The list must show `id` and `title`.

## Testing Steps

1. **RUN** `npm start list`
2. **OBSERVE** that you see a list of tasks.
