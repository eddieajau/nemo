# NEMO

A fishy play on "memo" (aka tasks).

## INSTALLATION

Clone the repo from

- `git@gitlab.com:eddieajau/nemo.git`, or
- `https://gitlab.com/eddieajau/nemo.git`

Install the dev dependencies using `npm` and then build the application.

```
npm i
npm run build
```

## RUN

Nemo is still growing up. Here's are the commands you can currently use.

### List all your tasks

To list all the tasks stored on disk, run the following command.

```
./nemo list
```

Use the `list` command to show you a list of your tasks. Each task is prefixed with the ID of the task. For example:

```
1: Test 1
2: Test 2
```

### Create a task

To create a new task, use the `create` command and just type the title you want for that task after it.

```
./nemo create What you want to do
```

After this you can run the `list` command to see your new task.

### Update a task

To update an existing task, use the `update` command. Start with the ID of the task (the number at the start of the row) and then type in the new title you want for that task.

```
./nemo update 1 The new title
```

After this you can run the `list` command to see your changes.

### Delete a task

To delete an existing task, use the `delete` command. Add the ID of the task you want to delete.

```
./nemo delete 1
```

After this you can run the `list` command to see your changes.

### Complete a task

To mark a task as complete, use the `complete` command. Add the ID of the task you want to delete.

```
./nemo complete 2
```

After this you can run the `list` command to see your changes. For example:

```
2: Test 2 [Complete]
3: What you want to do
```

## TESTS

To run the test suite, run the following command:

```
npm test
```

> WARNING: Running the tests will reset the data to a known test state.

## NOTES

- Data is stored in `/data.json`.
- This application was built and tested on Node v18.18.2.
