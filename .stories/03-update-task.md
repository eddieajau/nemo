# CLI: Update the title of a task

As a user
I need to be able to update an existing task
So that I can correct typos.

## Accecptance Criteria

1. Add an operation to update (replace) the `title` of a task.
   1. The user must supply the `id` of the task to update.
   2. Throw an error if the task does not exist.
   3. Throw an error if the task title is invalid.
   4. Should show a message if successful.
2. The data must persist between runs of the script.

## Testing Steps

1. **RUN** `npm start update 1 new title`
2. **RUN** `npm start list`
3. **OBSERVE** that you see the task has been updated.
4. **RUN** `npm start update 99 some title`
5. **OBSERVE** an error is thrown because there is no task with that ID.
6. **RUN** `npm start update 1`
7. **OBSERVE** an error is thrown because the title was missing.
