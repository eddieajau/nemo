/**
 * NemoError tests
 */

import { equal } from 'assert'
import { NemoError } from '../src/NemoError'

describe('NemoError', () => {
  it('should construct', () => {
    const error = new NemoError('the-error')

    equal(error.message, 'the-error')
    equal(error.name, 'NemoError')
  })
})
