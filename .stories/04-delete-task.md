# CLI: Delete a task

As a user
I need to be able to delete a task
Because I might not want to do it anymore.

## Accecptance Criteria

1. Add an operation to a task by `id`.
   1. Throw an error if the task does not exist.
   2. Should show a message if successful.
2. The data must persist between runs of the script.

## Testing Steps

1. **RUN** `npm start delete 1`
2. **RUN** `npm start list`
3. **OBSERVE** that you see the task has been updated.
4. **RUN** `npm start delete 99`
5. **OBSERVE** an error is thrown because there is no task with that ID.
