# CLI: Add a new task to the list

As a user
I need to be able to add new tasks
Because I need to do new things.

## Accecptance Criteria

1. Add an operation to add a new task to the list.
   1. It should throw an error if the task is empty.
   2. Should show a message if successful.
2. The `id` of the new task should be one plus the greatest existing ID.
3. The data must persist between runs of the script.

## Testing Steps

1. **RUN** `npm start create the title for the task`
2. **RUN** `npm start list`
3. **OBSERVE** that you see the task that you added.
4. **RUN** `npm start create`
5. **OBSERVE** an error is thrown because no title was supplied.
