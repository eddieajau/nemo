/**
 * TaskController Tests.
 */

import { deepEqual, equal, ok, rejects } from 'assert'
import { CommandController, LogInterface } from '../src/TaskController'
import { NemoError } from '../src/NemoError'
import { TaskService } from '../src/TaskService'

class OutputSpy implements LogInterface {
  public history: string[] = []

  public log(data?: string): void {
    this.history.push(data as string)
  }
}

describe('TaskController', () => {
  let tasksSpy: TaskService
  let outputSpy: OutputSpy
  let instance: CommandController

  beforeEach(() => {
    tasksSpy = {} as TaskService
    outputSpy = new OutputSpy()
    instance = new CommandController(tasksSpy, outputSpy)
  })

  describe('getArgs', () => {
    it('should get the args supplied to the CLI', () => {
      const argv = [
        '/nemo/node_modules/ts-node/dist/child/child-entrypoint.js',
        '/nemo/src/index.ts',
        'a',
        'test'
        //
      ]

      deepEqual(CommandController.getArgs(argv), ['a', 'test'])
    })
  })

  describe('route', () => {
    it('should throw if command unknown', async () => {
      await rejects(() => instance.route('unknown', []), { name: 'NemoError', message: 'Unknown command' })
    })
  })

  describe('list', () => {
    it('should list all the tasks', async () => {
      const cmd = 'list'
      const args: string[] = []
      let loaded = false

      tasksSpy.load = async () => {
        loaded = true
      }

      tasksSpy.list = async () => {
        return [
          { id: 2, title: 'Task 2', complete: false },
          { id: 3, title: 'Task 3', complete: true }
        ]
      }

      await instance.route(cmd, args)

      ok(loaded, 'the data was loaded')
      equal(outputSpy.history[0], '2: Task 2')
      equal(outputSpy.history[1], '3: Task 3 [Complete]')
    })
  })

  describe('create', () => {
    it('should add a new task', async () => {
      const cmd = 'create'
      const args = ['one', 'two', 'three']
      let loaded = false
      let created = false
      let saved = false

      tasksSpy.load = async () => {
        loaded = true
      }

      tasksSpy.create = async (title) => {
        equal(title, 'one two three')
        created = true

        return { id: 1, title, complete: false }
      }

      tasksSpy.save = async () => {
        saved = true
      }

      await instance.route(cmd, args)

      ok(loaded, 'should have loaded the data')
      ok(created, 'should have created the task')
      ok(saved, 'should have saved the task list')
      equal(outputSpy.history[0], 'Task created.')
    })
  })

  describe('update', () => {
    it('should update an existing task', async () => {
      const cmd = 'update'
      const args = ['1', 'new', 'title']
      let loaded = false
      let updated = false
      let saved = false

      tasksSpy.load = async () => {
        loaded = true
      }

      tasksSpy.update = async (id, title) => {
        equal(id, 1)
        equal(title, 'new title')
        updated = true
      }

      tasksSpy.save = async () => {
        saved = true
      }

      await instance.route(cmd, args)

      ok(loaded, 'should have loaded the data')
      ok(updated, 'should have updated the task')
      ok(saved, 'should have saved the task list')
      equal(outputSpy.history[0], 'Task updated.')
    })
  })

  describe('delete', () => {
    it('should delete a task', async () => {
      const cmd = 'delete'
      const args = ['1']
      let loaded = false
      let deleted = false
      let saved = false

      tasksSpy.load = async () => {
        loaded = true
      }

      tasksSpy.destroy = async (id) => {
        equal(id, 1)
        deleted = true
      }

      tasksSpy.save = async () => {
        saved = true
      }

      await instance.route(cmd, args)

      ok(loaded, 'should have loaded the data')
      ok(deleted, 'should have deleted the task')
      ok(saved, 'should have saved the task list')
      equal(outputSpy.history[0], 'Task deleted.')
    })
  })

  describe('complete', () => {
    it('should complete a task', async () => {
      const cmd = 'complete'
      const args = ['1']
      let loaded = false
      let completed = false
      let saved = false

      tasksSpy.load = async () => {
        loaded = true
      }

      tasksSpy.complete = async (id) => {
        equal(id, 1)
        completed = true
      }

      tasksSpy.save = async () => {
        saved = true
      }

      await instance.route(cmd, args)

      ok(loaded, 'should have loaded the data')
      ok(completed, 'should have completed the task')
      ok(saved, 'should have saved the task list')
      equal(outputSpy.history[0], 'Task completed.')
    })
  })
})
