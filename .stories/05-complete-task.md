# CLI: Mark a task a complete.

As a user
I need to be able to mark tasks as complete
So that I don't do things twice.

## Accecptance Criteria

1. Add a new property to the Task object model:
   1. `complete`: boolean
2. Add an operation to mark a task as completed.
   1. Throw an error if the task does not exist.
   2. Should show a message if successful.
   3. New tasks default to `false`.
3. If a task is complete, add `[Complete]` to the end of the row.
4. The data must persist between runs of the script.

## Testing Steps

1. **RUN** `npm start complete 1`
2. **RUN** `npm start list`
3. **OBSERVE** that you see the task has been completed.
4. **RUN** `npm start complete 99`
5. **OBSERVE** an error is thrown because there is no task with that ID.
