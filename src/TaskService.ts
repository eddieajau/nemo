/**
 * TaskService
 */

const { rootPath } = require('get-root-path')
import { readFile, writeFile } from 'fs/promises'
import { join } from 'path'
import { Task } from './types'
import { NemoError } from './NemoError'

const DATA_PATH = join(rootPath, 'data.json')
export const ERROR_MISSING_TITLE = 'Task must have a title'
export const ERROR_NOT_FOUND = 'Task not found'

export class TaskService {
  private tasks: Map<number, Task>

  constructor() {
    this.tasks = new Map()
  }

  /**
   * Mark a task as complete by ID.
   */
  async complete(id: number) {
    const task = this.tasks.get(id)

    if (!task) {
      throw new NemoError(ERROR_NOT_FOUND)
    }

    task.complete = true
    this.tasks.set(id, task)
  }

  /**
   * Create a new task.
   */
  async create(title: string): Promise<Task> {
    const id = this.getNextId()
    const task = { id, title, complete: false }

    this.validate(task)
    this.tasks.set(task.id, task)

    return task
  }

  /**
   * Get the number of tasks.
   */
  async count(): Promise<number> {
    return this.tasks.size
  }

  /**
   * Delete a task permanently from the list.
   */
  async destroy(id: number) {
    if (!this.tasks.delete(id)) {
      throw new NemoError(ERROR_NOT_FOUND)
    }
  }

  /**
   * Get a list of the tasks.
   */
  async list(): Promise<Task[]> {
    return [...this.tasks.values()]
  }

  /**
   * Load the tasks from disk.
   */
  async load() {
    const buffer = await readFile(DATA_PATH)
    const tasks = JSON.parse(buffer.toString())

    this.tasks.clear()

    for (var task of tasks) {
      this.tasks.set(task.id, task)
    }
  }

  /**
   * Reset the internal task list.
   */
  async reset() {
    this.tasks.clear()
  }

  /**
   * Save the tasks to disk.
   */
  async save() {
    await writeFile(DATA_PATH, JSON.stringify([...this.tasks.values()], null, 2))
  }

  /**
   * Update the title of a task by ID.
   */
  async update(id: number, title: string) {
    await this.validate({ id, title })

    const task = this.tasks.get(id)

    if (!task) {
      throw new NemoError(ERROR_NOT_FOUND)
    }

    task.title = title
    this.tasks.set(id, task)
  }

  /**
   * Get the next ID for a new Task. 1 + the maximum already in the map.
   */
  private getNextId() {
    let max = 0

    this.tasks.forEach((t) => {
      if (t.id > max) {
        max = t.id
      }
    })

    return max + 1
  }

  /**
   * Validate a task object.
   */
  private validate(task: Partial<Task>) {
    if (!task.title) {
      throw new NemoError(ERROR_MISSING_TITLE)
    }
  }
}
