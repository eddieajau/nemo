/**
 * CLI Command Controller.
 */

import { NemoError } from './NemoError'
import { TaskService } from './TaskService'

export interface LogInterface {
  log: (data?: string) => void
}

export class CommandController {
  /**
   * A helper method to find the args passed assuming argv is used.
   */
  public static getArgs(args: string[]) {
    const results = [...args]

    while (results.length > 0) {
      const arg = results.shift() as string

      if (arg.endsWith('index.ts') || arg.endsWith('index.js')) {
        // Found the script entrypoint. Everything else
        break
      }
    }

    return results
  }

  constructor(private tasks: TaskService, private logger: LogInterface) {}

  /**
   * Command router.
   */
  public async route(command: string, args: string[]) {
    if (command === 'list') {
      return this.list(args)
    } else if (command === 'create') {
      return this.create(args)
    } else if (command === 'update') {
      return this.update(args)
    } else if (command === 'delete') {
      return this.destroy(args)
    } else if (command === 'complete') {
      return this.complete(args)
    }

    throw new NemoError('Unknown command')
  }

  /**
   * Handle the complete command.
   */
  private async complete(args: string[]) {
    const [id] = args

    await this.tasks.load()
    await this.tasks.complete(Number(id))
    await this.tasks.save()
    this.logger.log('Task completed.')
  }

  /**
   * Handle the create command
   */
  private async create(args: string[]) {
    await this.tasks.load()
    await this.tasks.create(args.join(' '))
    await this.tasks.save()
    this.logger.log('Task created.')
  }

  /**
   * Handle the update command.
   */
  private async destroy(args: string[]) {
    const [id] = args

    await this.tasks.load()
    await this.tasks.destroy(Number(id))
    await this.tasks.save()
    this.logger.log('Task deleted.')
  }

  /**
   * Handle the list command.
   */
  private async list(args: string[]) {
    await this.tasks.load()

    const tasks = await this.tasks.list()

    tasks.forEach((t) => {
      this.logger.log(`${t.id}: ${t.title}${t.complete ? ' [Complete]' : ''}`)
    })
  }

  /**
   * Handle the update command.
   */
  private async update(args: string[]) {
    const [id, ...rest] = args

    await this.tasks.load()
    await this.tasks.update(Number(id), rest.join(' '))
    await this.tasks.save()
    this.logger.log('Task updated.')
  }
}
