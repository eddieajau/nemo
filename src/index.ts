/**
 * Task CLI
 */

import { CommandController } from './TaskController'
import { NemoError } from './NemoError'

//
import { TaskService } from './TaskService'
;(async function main() {
  const taskService = new TaskService()
  const controller = new CommandController(taskService, console)
  const [command, ...args] = CommandController.getArgs(process.argv)

  try {
    await controller.route(command, args)
  } catch (err: unknown) {
    if (err instanceof NemoError) {
      console.error(`\nOops, something went wrong: ${err.message}\n`)
    } else {
      throw err
    }
  }
})().catch((err) => {
  console.error('===== FATAL ERROR =====')
  console.log(new Date())
  console.error(err)
  process.exit(1)
})
