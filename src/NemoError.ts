/**
 * NemoError
 */

export class NemoError {
  public name = 'NemoError'

  constructor(public message: string) {}
}
