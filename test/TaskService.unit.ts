/**
 * TaskService unit tests.
 */

import { equal, ok, rejects } from 'assert'
import { ERROR_MISSING_TITLE, ERROR_NOT_FOUND, TaskService } from '../src/TaskService'
import { NemoError } from '../src/NemoError'

describe('TaskService', () => {
  let instance: TaskService

  beforeEach(() => {
    instance = new TaskService()
  })

  describe('reset', () => {
    it('should reset the internal task list', async () => {
      const count = await instance.count()
      await instance.create('one')
      await instance.create('two')
      await instance.create('three')
      equal(await instance.count(), count + 3)

      await instance.reset()
      equal(await instance.count(), 0)
    })
  })

  describe('complete', () => {
    it('should set a task to complete', async () => {
      const task = await instance.create('one')

      equal(task.complete, false, 'should be not complete')

      await instance.complete(task.id)

      const tasks = await instance.list()
      const changed = tasks.filter((t) => t.id === task.id).pop()

      equal(changed?.id, task.id)
      equal(changed?.complete, true)
    })

    it('should throw if ID is not found', async () => {
      return rejects(() => instance.complete(99), {
        name: 'NemoError',
        message: ERROR_NOT_FOUND
      })
    })
  })

  describe('create', () => {
    it('should create a task', async () => {
      const title = 'the new title'
      const result = await instance.create(title)

      equal(result.id, 1)
      equal(result.title, title)
      equal(result.complete, false, 'should default to false')
    })

    it('should throw if title empty', async () => {
      return rejects(() => instance.create(''), {
        name: 'NemoError',
        message: ERROR_MISSING_TITLE
      })
    })
  })

  describe('destroy', () => {
    it('should delete a task by ID', async () => {
      await instance.create('one')
      await instance.create('two')
      await instance.create('three')
      await instance.destroy(1)

      const tasks = await instance.list()
      const removed = tasks.every((t) => t.id !== 1)

      equal(await instance.count(), 2, 'should be two tasks')
      equal(removed, true, 'should have been removed')
    })

    it('should throw if ID is not found', async () => {
      return rejects(() => instance.destroy(99), {
        name: 'NemoError',
        message: ERROR_NOT_FOUND
      })
    })
  })

  describe('list', () => {
    it('should list the loaded data', async () => {
      equal(await instance.count(), 0, 'should be empty')

      await instance.create('one')

      const result = await instance.list()

      equal(result[0].title, 'one', 'should return the list')
    })
  })

  describe('load', () => {
    it('should load the data file', async () => {
      equal(await instance.count(), 0, 'should be empty')

      await instance.load()

      ok((await instance.count()) >= 1, 'should be something in the file')
    })
  })

  describe('update', () => {
    it('should update a task', async () => {
      const task = await instance.create('Test 1')

      await instance.create('Test 2')
      await instance.create('Test 3')
      await instance.update(task.id, 'Test 1 changed')

      const tasks = await instance.list()
      const changed = tasks.filter((t) => t.id === task.id).pop()

      equal(changed?.title, 'Test 1 changed', 'should be changed')
    })

    it('should throw if the new title is invalid', async () => {
      return rejects(() => instance.update(0, ''), {
        name: 'NemoError',
        message: ERROR_MISSING_TITLE
      })
    })

    it('should throw if ID is not found', async () => {
      return rejects(() => instance.update(99, 'good title'), {
        name: 'NemoError',
        message: ERROR_NOT_FOUND
      })
    })
  })

  describe('save', () => {
    it('should save the data to disk', async () => {
      await instance.create('Test 1')
      await instance.create('Test 2')
      await instance.save()
      await instance.load()
      equal(await instance.count(), 2)
    })
  })
})
