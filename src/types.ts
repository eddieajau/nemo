/**
 * Type definitions for the Task domain models.
 */

export type Task = {
  id: number
  title: string
  complete: boolean
}
